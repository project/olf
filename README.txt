

--- README  -------------------------------------------------------------

Outgoing Links Filter
 
This is a simple filter which adds a configurable CSS class name to all outgoing links
on your site (e.g., links that to point to a different website).

Written by Ted Serbinski, aka, m3avrck
  hello@tedserbinski.com
  http://tedserbinski.com
  


--- INSTALLATION --------------------------------------------------------

1. Place the olf folder in your modules directory

2. Enable "olf" under administer > modules

4. Goto administer > input formats, select a desired input format and "configure" it

5. Enable the "outgoing links filter"

6. Click on the "configure" tab for the filter and specify a CSS class name to apply to 
   all outgoing links